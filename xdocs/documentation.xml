<?xml version="1.0" encoding="ISO-8859-1" ?>
<!-- 
/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 -->

<document>

  <properties>
    <title>commons-invoke</title>
  </properties>

  <body>
	<section name="About commons-invoke">  	
  	<p>
  		1. <a href="#the_framework">The invocation framework</a><br/>
  		2. <a href="#usage">Usage</a>
  	</p>
  	</section>
  	<section name="The invocation framework">  	
  	<p>
  		<a name="the_framework"/>
		The invocation framework defines the Invocable interface which represent an object that has 
        InvocableFunctions that can be invoked. There are a few uses for the framework:
        <ol>
            <li>As a wrapper framework for reflection for optimizing method invocation. For example, the 
                AbstractInvocable uses the reflection for method invocation by default, but can optionally 
                take implemented InvocableFunction for doing the compiled method invocation
            </li>
            <li>
                As a wrapper framework for reflection for proxying method invocation. For example, the 
                AbstractInvocable uses the reflection for method invocation but optionally take implemented 
                InvocableFunction to proxy the reflection call
            </li>
            <li>
                As arbitrary invocation framework for application independant of reflection (e.g. defines a set 
                of functions on an object independant of what java methods the object really provides)
            </li>
        </ol>        		
	</p>
	</section>    
	<section name="Example">
		<p>
		<a name="example"/>
        <subsection name="Using common-invoke as reflection wrapper">
            <p>
                Let supposed that you have a method <i>callMeALot</i> in an class <i>MyObject </i>that is called frequently in your application, 
                but as the requirement of the design you are allowed to call it using java reflection api
                <source><![CDATA[
MyObject obj = new MyObject();

//...later somewhere in your application...
Method m = obj.getMethod("callMeALot", argTypes);
m.invoke(args);
                ]]></source>
                Supposed that method <i>callMeALot</i> is called a lot (10000 per seconds) and the reflection call the slowing down the application.
                The following code shows how to replace the java reflection api with the common-invoke
                <source><![CDATA[
MyObject obj = new MyObject();
Invocable inv = new ReflectionWrapperInvocable(obj);

//...later somewhere in your application
inv.invoke("callMeALot", args); //notice this is simpler than reflection api
                ]]></source>       
                The last line in the source above still uses the java reflection api to make the action method call the <i>callMeALot</i>.
                But now, the method can be optimize by doing the following
                <source><![CDATA[
MyObject obj = new MyObject();
Invocable inv = new ReflectionWrapperInvocable(obj);
inv.addFunction(new AbstractInvocableFunction("callMeALot", new Class[] {}){
    public Object invoke(Object obj, Object[] args){
        return ((MyObject)obj).callMeALot();
    }
}

//...later somewhere in your application
inv.invoke("callMeALot", args); //this time, it will do the direct call
                ]]></source>                       
                While the java reflection api is in general fast, the benchmark shows that the optimization uses
                half the time required by the java reflection api. 
            </p>                 
        </subsection>  
		</p>
	</section>
  </body>
</document>
