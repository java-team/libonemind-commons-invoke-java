/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.invoke;

import java.lang.reflect.Method;
import org.onemind.commons.java.lang.reflect.ReflectUtils;
import junit.framework.TestCase;
/**
 * TODO comment
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: BenchmarkTest.java,v 1.2 2004/08/27 04:01:39 thlee Exp $ $Name:  $
 */
public class BenchmarkTest extends TestCase
{

    private final MyObject obj = new MyObject();

    private final Class argTypes[] = {};

    private final Object args[] = {};

    private final ReflectionWrapperInvocable invocable = new ReflectionWrapperInvocable(obj);

    private final ReflectionWrapperInvocable optimizedInvocable = new ReflectionWrapperInvocable(obj);

    public class MyObject
    {

        public void callMeALot()
        {
            //do nothing 
        }
    }

    public void setUp() throws Exception
    {
        //have it initialized first to avoid penalty in wrapper call
        ReflectUtils.getClass("java.lang.Object");
        optimizedInvocable.addFunction(new AbstractInvocableFunction("callMeALot")
        {

            /** 
             * {@inheritDoc}
             */
            public Object invoke(Object target, Object[] args) throws Exception
            {
                // TODO Auto-generated method stub
                ((MyObject) target).callMeALot();
                return null;
            }
        });
    }

    private long _callReflect(long iteration) throws Exception
    {
        long start = System.currentTimeMillis();
        Method m = obj.getClass().getMethod("callMeALot", argTypes);
        for (int i = 0; i < iteration; i++)
        {
            m.invoke(obj, args);
        }
        return System.currentTimeMillis() - start;
    }

    private long _callInvoke(long iteration) throws Exception
    {
        long start = System.currentTimeMillis();
        for (int i = 0; i < iteration; i++)
        {
            invocable.invoke("callMeALot", args);
        }
        return System.currentTimeMillis() - start;
    }

    private long _callInvokeOptimized(long iteration) throws Exception
    {
        long start = System.currentTimeMillis();
        for (int i = 0; i < iteration; i++)
        {
            optimizedInvocable.invoke("callMeALot", args);
        }
        return System.currentTimeMillis() - start;
    }

    public void testInvoke() throws Exception
    {
        int iterationCount = 10000;
        long t1 = 0, t2 = 0, t3 = 0;
        for (int i=0; i<iterationCount; i++)
        {
            t1 += _callReflect(1);
            t2 += _callInvoke(1);
            t3 += _callInvokeOptimized(1);
        }
        System.out.println("Time for reflect call: " + t1);
        System.out.println("Time for wrapped reflect call: " + t2);
        System.out.println("Time for optimized invoke call: " + t3);
    }
    
    //for profiling
    public static void main(String args[]) throws Exception
    {
        BenchmarkTest test =new BenchmarkTest();
        test.setUp();
        test.testInvoke();
    }
}