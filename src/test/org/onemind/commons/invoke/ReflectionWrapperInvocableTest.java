/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.invoke;

import junit.framework.TestCase;
/**
 * Unit test of WrapperInvocable
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: ReflectionWrapperInvocableTest.java,v 1.4 2004/08/27 02:35:04 thlee Exp $ $Name:  $
 */
public class ReflectionWrapperInvocableTest extends TestCase
{

    /**
     * Generic test
     * @throws Exception if there's problem
     */
    public void testInvoke() throws Exception
    {
        ReflectionWrapperInvocable wrapper = new ReflectionWrapperInvocable(new String("object"));
        wrapper.addFunction(new AbstractInvocableFunction("getA")
        {

            public Object invoke(Object target, Object[] args) throws InvocationException
            {
                return "a";
            }
        });
        assertEquals("a", (wrapper.invoke("getA", null)));
        assertEquals("object", wrapper.invoke("toString", null));
    }
    


    /**
     * Generic test
     * @throws Exception if there's problem
     */
    public void testArgCheck() throws Exception
    {
        ReflectionWrapperInvocable wrapper = new ReflectionWrapperInvocable(new String("object"));
        wrapper.addFunction(new AbstractInvocableFunction("getA")
        {

            public Object invoke(Object target, Object[] args) throws InvocationException
            {
                return "a";
            }
        });
        assertNotNull(wrapper.getFunction("getA", null));        
        assertNull(wrapper.getFunction("getA", new Object[] { "a" }));        
    }
}