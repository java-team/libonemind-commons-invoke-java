/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.invoke;

import org.onemind.commons.java.lang.reflect.ReflectUtils;
/**
 * An abstract implementation of InvocableFunction
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: AbstractInvocableFunction.java,v 1.6 2005/01/24 05:51:54 thlee Exp $ $Name:  $
 */
public abstract class AbstractInvocableFunction implements InvocableFunction
{

    /** the name **/
    private String _name;

    /** the method **/
    private Class[] _argTypes;
    
    /**
     * Constructor
     * @param name the name
     */
    public AbstractInvocableFunction(String name)
    {
        this(name, null);
    }

    /**
     * Constructor
     * @param name the name
     * @param argTypes the argument types
     */
    public AbstractInvocableFunction(String name, Class[] argTypes)
    {
        _name = name;
        _argTypes = argTypes;
    }

    /**
     * {@inheritDoc}
     */
    public boolean canInvokeOn(Class[] argTypes)
    {
        return ReflectUtils.isCompatible(_argTypes, argTypes);
    }

    /**
     * Return the argTypes
     * @return the argTypes.
     */
    public final Class[] getArgTypes()
    {
        return _argTypes;
    }

    /**
     * Return the name
     * @return the name.
     */
    public final String getName()
    {
        return _name;
    }
    
    /** 
     * {@inheritDoc}
     */
    public String toString()
    {
        return ReflectUtils.toMethodString(_name, _argTypes);
    }
}