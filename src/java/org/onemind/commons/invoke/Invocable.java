/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */
package org.onemind.commons.invoke;


/**
 * An interface for a object provide information about
 * functions that can be invoked on itself (through a key)
 * and API for invoke the method on on itself
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: Invocable.java,v 1.2 2004/08/26 16:08:23 thlee Exp $ $Name:  $
 */
public interface Invocable
{
    /**
     * whether the function can be invoke with the arguments
     * @param functionName the function name
     * @param args the arguments
     * @return true if can invoke
     */
    public boolean canInvoke(String functionName, Object[] args);

    /**
     * Get the function
     * @param functionName the function name
     * @param args the arguments
     * @return the function, or null if the function cannot be found
     */
    public InvocableFunction getFunction(String functionName, Object[] args);
    
    /**
     * invoke the function with the arguments provided
     * @param functionName the function name
     * @param args the arguments
     * @return the object returns from the invocation
     * @throws Exception if there's problem invoking the function
     */
    public Object invoke(String functionName, Object[] args)
        throws Exception;
}
