/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */
package org.onemind.commons.invoke;


/**
 * Represents a function that can be invoked
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: InvocableFunction.java,v 1.4 2004/08/27 02:59:39 thlee Exp $ $Name:  $
 */
public interface InvocableFunction
{
    /**
     * Invoke the function on the target object with argument args
     * @param target the target
     * @param args the arguments
     * @return application specific object
     * @throws Exception if there's problem completing the invocation
     */
    public Object invoke(Object target, Object[] args)
        throws Exception;
    
    /**
     * Get the name
     * @return the name
     */
    public String getName();
    
    /**
     * Get the argument types
     * @return the argument type
     */
    public Class[] getArgTypes();
    
    /**
     * Whether this method can be invoke on the given arguments
     * @return true if can invoke on the arguments
     */
    public boolean canInvokeOn(Class args[]);
    
}
