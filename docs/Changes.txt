commons-invoke-1.1.0
	Jan 23, 2005; TiongHiang Lee (thlee@onemindsoft.org)
	- Refactor DefaultInvocable to AbstractInvocable
	
commons-invoke-1.0.0
	Aug 01, 2004; TiongHiang Lee (thlee@onemindsoft.org)
	- Fix AbstractInvocable.addFunction() to not exclude name since name is built into the function now.
	
	
